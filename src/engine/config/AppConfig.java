//====================================
//  KYLE RUSSELL
//  13831056
//  PDC Project
//====================================

package engine.config;


public class AppConfig
{

    //---------------------------------------------------------------------
    //                              CONFIG KEYS
    //---------------------------------------------------------------------
    public static final boolean DEBUG_MODE      =   false;
    public static final boolean GUI_MODE        =   true;
    public static final String APP_NAME         =   "StudentCore";
    public static final int WINDOW_WIDTH        =   768;
    public static final int WINDOW_HEIGHT       =   648;
    public static final String RESOURCE_DIR     =   "/root/Dropbox/PDCAssignmentImages/";
    public static final boolean ALLOW_CRED_SAVE =   true;
    public static final int NOTIFICATION_TIME   =   30000;
    public static final String CRED_SAVE_FILE   =   "SavedUsers.txt";
}
