//====================================
//  KYLE RUSSELL
//  13831056
//  PDC Project
//====================================

package engine.config;

public class DatabaseConfig
{ 
    //-------------------------------------------------------
    //                      CONFIG KEYS
    //-------------------------------------------------------
    public static final String SERVER           =   "localhost"; //database server
    public static final int PORT                =   1527; //database port - default: 1527
    public static final String DB_USERNAME      =   "kyleruss"; //database server username
    public static final String DB_PASSWORD      =   "fgsmg2"; //database server password
    public static final String DATABASE         =   "School"; //active using database
    public static final String DRIVER           =   "jdbc:derby"; //db connection driver
    public static final String SCHEMA           =   "APP";
    public static final String DEFAULT_KEY      =   "id";
}
