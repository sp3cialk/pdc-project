//====================================
//  KYLE RUSSELL
//  13831056
//  PDC Project
//====================================

package engine.config;

import java.util.Properties;

public class AuthConfig 
{


    //---------------------------------------------------------------------
    //                              CONFIG KEYS
    //---------------------------------------------------------------------
    public static final String AUTH_TABLE           =   "users";
    public static final String HASH_ALGORITHM       =   "SHA-1";
    public static final String ENC_FORMAT           =   "utf-8";
    public static final String USERNAME_COL         =   "username";
    public static final String PASSWORD_COL         =   "password";
    public static final String SALT_PREFIX          =   "vb+FwcR~Sj+bq5imRBJd3%L";
}
